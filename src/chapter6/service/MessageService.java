package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;


public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//●特定のつぶやきを見たいために追加した文
	//selectの引数にString型のuserIdを追加
	public List<UserMessage> select(String userId, String startDate, String endDate) {
		final int LIMIT_NUM = 1000;
		String defaultStartDate = "2020-01-01 00:00:00";
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String defaultEndDate = sdf.format(calendar.getTime());

		Connection connection = null;
		try {
			connection = getConnection();

			if(!StringUtils.isBlank(startDate)) {
				startDate = startDate + " 00:00:00";
			}else {
				startDate = defaultStartDate;
			}
			if(!StringUtils.isBlank(endDate)) {
				endDate = endDate + " 23:59:59";
			}else {
				endDate = defaultEndDate;
			}

			//idをnullで初期化
			//ServletからuserIdの値が渡ってきていたら
			//整数型に型変換し、idに代入
			Integer id = null;
			if(!StringUtils.isBlank(userId)) {
				id = Integer.parseInt(userId);
			}

			//messageDao.selectに引数としてInteger型のidを追加
			//idがnullだったら全件取得する
			//idがnull以外だったら、その値に対応するユーザーIDの投稿を取得する
			List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, startDate, endDate);
			commit(connection);

			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//●つぶやき削除
	public void delete(int id) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().delete(connection, id);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//●つぶやき編集画面へ遷移
	public Message select(int editId) {

		Connection connection = null;
		try {
			connection = getConnection();
			Message message = new MessageDao().select(connection, editId);
			commit(connection);
			return message;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//●つぶやき編集
	public void update(Message editMessage) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().update(connection, editMessage);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}