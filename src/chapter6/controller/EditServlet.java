package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {
	//つぶやき編集画面へ遷移
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		//Listの初期化、パラメータ取得、messageの初期化
		List<String> errorMessages = new ArrayList<String>();
		String id = request.getParameter("id");
		Message message = null;

		//idが空白ではない且つidが数字の場合
		//selectをしてみよう
		if(!StringUtils.isBlank(id) && (id.matches("^[0-9]+$"))) {
			int editId = Integer.parseInt(id);
			message = new MessageService().select(editId);
		}

		//存在しないid＝つぶやきが存在しない場合
		if(message == null){
			errorMessages.add("不正なパラメータが入力されました");
			request.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}
		request.setAttribute("message", message);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	//つぶやき編集を更新
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		String id = request.getParameter("id");
		int editId = Integer.parseInt(id);

		String editText = request.getParameter("text");

		if (!isValid(editText, errorMessages)) {
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		Message message = new Message();
		message.setText(editText);
		message.setId(editId);

		new MessageService().update(message);
		response.sendRedirect("./");
	}

	private boolean isValid(String editText, List<String> errorMessages) {

		if (StringUtils.isBlank(editText)) {
			errorMessages.add("メッセージを入力してください");
		} else if (140 < editText.length()) {
			errorMessages.add("140文字以下で入力してください");
		}
		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
