package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/deleteMessage" })

public class DeleteMessageServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		//jspからパラメーターを受け取る
		//Serviceに渡すために、String型からint型へ変換
		String id = request.getParameter("id");
		int deleteId = Integer.parseInt(id);

		//Servicenに渡す
		new MessageService().delete(deleteId);
		response.sendRedirect("./");
	}
}
